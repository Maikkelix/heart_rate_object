import { Component } from '@angular/core';
import { HrService } from '../hr.service';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  age: number;

  constructor(private hrService: HrService) {}

  calculate() {
    this.hrService.setAge(this.age);
    this.hrService.calculate();
  }

}
